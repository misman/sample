package com.misman.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.misman.listeners.DayOffListener;
import com.misman.model.Cryptor;
import com.misman.model.DayOff;
import com.misman.staffoperations.LoginActivity;
import com.misman.staffoperations.OpdetailActivity;
import com.misman.staffoperations.OperationsActivity;
import com.misman.staffoperations.R;
import com.misman.tool.GeneralAdapter;
import com.misman.tool.Http;
import com.misman.tool.HttpRes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Created by misman on 11.3.2016.
 */
public class DayoffUsed extends Fragment implements DayOffListener {

    private AsyncTask<String,String,HttpRes> getDayOffTask;
    private RecyclerView listDayOff;
    private long mLastClickTime=0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dayoff_used,container,false);
        listDayOff = (RecyclerView) view.findViewById(R.id.list_dayoff_demand);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        refreshList();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getDayOffTask!=null && !getDayOffTask.isCancelled())
            getDayOffTask.cancel(true);
    }

    @Override
    public String checkDayoffList(Date start, Date finish) {
        return null;
    }

    @Override
    public void onItemClick(int position) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

    }

    @Override
    public void refreshList() {
        getDayOffTask = new AsyncTask<String, String, HttpRes>() {
            @Override
            protected HttpRes doInBackground(String... params) {
                String urlMake = LoginActivity.BASE_URL+
                        OperationsActivity.personel.Pers_Code+"/A/restlist";
                Http http = new Http(urlMake, Http.RQ.GET);
                return http.startReq();
            }

            @Override
            protected void onPostExecute(HttpRes httpRes) {
                super.onPostExecute(httpRes);
                if (httpRes!=null) {
                    String jsonDayOffs = Cryptor.decrypt(httpRes.getData());
                    if (httpRes.getResponse() == 200) {
                        ArrayList<DayOff> dayOffs = OpdetailActivity.gson.fromJson(jsonDayOffs,
                                new TypeToken<ArrayList<DayOff>>(){}.getType());
                        if (dayOffs!=null && !dayOffs.isEmpty()){
                            Collections.sort(dayOffs);
                            listDayOff.setLayoutManager(new LinearLayoutManager(getActivity()));
                            listDayOff.setAdapter(new GeneralAdapter(DayoffUsed.this, dayOffs));
                        }else {
                            Toast.makeText(getActivity(),"İzin Bilgisi Bulunmamaktadır",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(getActivity(),"Sunucuda hata oluştu",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };
        getDayOffTask.execute();
    }
}
