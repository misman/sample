package com.misman.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.misman.listeners.TimePickListen;
import com.misman.model.Config;
import com.misman.staffoperations.R;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by misman on 24.06.2015.
 */
public class DatetimePick extends DialogFragment {

    public static final String BUNDLE_IS_FINISH = "is_finish";
    public static final String BUNDLE_DATE = "date";
    private boolean isFinish;
    private Date startDate;
    private TimePickListen listen;

    public static DatetimePick newInstance(boolean isFinish, String startDate){
        Bundle bundle = new Bundle();
        bundle.putBoolean(BUNDLE_IS_FINISH, isFinish);
        if (isFinish)
            bundle.putString(BUNDLE_DATE,startDate);
        DatetimePick datetimePick = new DatetimePick();
        datetimePick.setArguments(bundle);
        return datetimePick;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listen = (TimePickListen) getTargetFragment();
        Bundle bundle = getArguments();

        isFinish = bundle.getBoolean(BUNDLE_IS_FINISH);
        if (isFinish) {
            try {
                startDate = Config.sdf2.parse(bundle.getString(BUNDLE_DATE));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.datetime, null);
        TextView txtTitle = (TextView) view.findViewById(R.id.txt_title);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.date_picker);
        final TimePicker timePicker = (TimePicker) view.findViewById(R.id.time_picker);

        int year;
        int month;
        int day;
        int hour;
        int minutes;

        Calendar inspectedAt = Calendar.getInstance();
        if (!isFinish) {
            txtTitle.setText(R.string.dt_start_date);
            year    = inspectedAt.get(Calendar.YEAR) ;
            month   = inspectedAt.get(Calendar.MONTH);
            day     = inspectedAt.get(Calendar.DAY_OF_MONTH);
            hour    = 8;

            minutes = 0;
            day += 1;
        }else {
            txtTitle.setText(R.string.dt_finish_date);
            inspectedAt.setTime(startDate);
            hour = inspectedAt.get(Calendar.HOUR_OF_DAY);
            year    = inspectedAt.get(Calendar.YEAR) ;
            month   = inspectedAt.get(Calendar.MONTH);
            day     = inspectedAt.get(Calendar.DAY_OF_MONTH)+1;
            minutes = inspectedAt.get(Calendar.MINUTE);
        }

        timePicker.setIs24HourView(true);
        timePicker.setCurrentHour(hour);
        timePicker.setCurrentMinute(minutes);
        datePicker.updateDate(year, month, day);

        builder.setPositiveButton(R.string.general_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int selHour = timePicker.getCurrentHour();
                int selMinute = timePicker.getCurrentMinute();

                int selYear = datePicker.getYear();
                int selMounth = datePicker.getMonth();
                int selDay = datePicker.getDayOfMonth();

                Calendar selCal = Calendar.getInstance();
                selCal.set(selYear, selMounth, selDay, selHour, selMinute);
                if (isFinish) {
                    listen.setFinishTime(selCal.getTime());
                } else {
                    listen.setStartTime(selCal.getTime());
                }
            }
        });

        builder.setNegativeButton(R.string.general_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });
        builder.setView(view);
        final AlertDialog alert = builder.create();
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                int height = getResources().getDimensionPixelSize(R.dimen.frag_date_btn_height);
                int fontSize = getResources().getDimensionPixelSize(R.dimen.frag_btn_font_size);
                Button btnPositive = alert.getButton(Dialog.BUTTON_POSITIVE);
                btnPositive.setHeight(height);
                btnPositive.setTextSize(fontSize);

                Button btnNegative = alert.getButton(Dialog.BUTTON_NEGATIVE);
                btnNegative.setHeight(height);
                btnNegative.setTextSize(fontSize);
            }
        });

        return alert;
    }

}
