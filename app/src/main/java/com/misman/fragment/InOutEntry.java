package com.misman.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.misman.listeners.DetailListener;
import com.misman.listeners.InOutListener;
import com.misman.model.Config;
import com.misman.model.Cryptor;
import com.misman.model.InOut;
import com.misman.staffoperations.LoginActivity;
import com.misman.staffoperations.OpdetailActivity;
import com.misman.staffoperations.OperationsActivity;
import com.misman.staffoperations.R;
import com.misman.tool.Http;
import com.misman.tool.HttpRes;
import com.misman.tool.InOutListAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

/**
 * Created by misman on 29.03.2016.
 */
public class InOutEntry extends Fragment implements InOutListener {

    private AsyncTask<String,String,HttpRes> getInOut;
    private ListView listView;
    private long mLastClickTime=0;
    private CircularProgressBar progBar;
    public static final String BUNDLE_EXCUSE ="b_e";
    public static final String BUNDLE_XID ="b_xid";
    private DetailListener listener;
    private Date beginDate, endDate;
    private TextView txtInterval;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inout_entry,container,false);
        listView = (ListView) view.findViewById(R.id.list_in_out);
        txtInterval = (TextView) view.findViewById(R.id.txt_interval);
        RelativeLayout btnPre = (RelativeLayout) view.findViewById(R.id.btn_pre_month);
        RelativeLayout btnNext = (RelativeLayout) view.findViewById(R.id.btn_next_month);
        btnPre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preMonth();
                refreshTimer();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextMonth();
                refreshTimer();
            }
        });
        progBar = (CircularProgressBar) view.findViewById(R.id.progress_bar);
        listener = (DetailListener) getActivity();
        return view;
    }

    private void inOutRequest(String beginDate, String endDate){
        getInOut = new AsyncTask<String, String, HttpRes>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progBar.setVisibility(View.VISIBLE);
                listView.setVisibility(View.INVISIBLE);
            }

            @Override
            protected HttpRes doInBackground(String... params) {
                String url = LoginActivity.BASE_URL + OperationsActivity.personel.Pers_Code
                        + "/" + params[0] + "/" + params[1] + "/pdkslist";
                Http http = new Http(url, Http.RQ.GET);
                HttpRes res = http.startReq();
                if (res!=null){
                    res.setData(Cryptor.decrypt(res.getData()));
                }
                return res;
            }

            @Override
            protected void onPostExecute(HttpRes httpRes) {
                super.onPostExecute(httpRes);
                if (httpRes!=null){
                    if (httpRes.getResponse()==200){
                        ArrayList<InOut> inOuts = OpdetailActivity.gson.
                                fromJson(httpRes.getData(),
                                new TypeToken<ArrayList<InOut>>(){}.getType());
                        System.out.println("DATA " + httpRes.getData());
                        listView.setAdapter(new InOutListAdapter(getActivity(),InOutEntry.this,inOuts));
                        progBar.setVisibility(View.INVISIBLE);
                        listView.setVisibility(View.VISIBLE);
                        updateIntervalTxt();
                    }else {
                        Toast.makeText(getActivity(),"Veri Problemi",
                                Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(),"Servis Erişim Problemi",
                            Toast.LENGTH_SHORT).show();
                }
            }
        }.execute(beginDate,endDate);
    }

    @Override
    public void onStart() {
        super.onStart();
        refreshList();
    }

    private void nextMonth(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DAY_OF_YEAR, 30);
        endDate = calendar.getTime();
        calendar.setTime(beginDate);
        calendar.add(Calendar.DAY_OF_YEAR, 30);
        beginDate = calendar.getTime();
        inOutRequest(Config.sdf3.format(beginDate),Config.sdf3.format(endDate));
    }

    private void preMonth(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DAY_OF_YEAR, -30);
        endDate = calendar.getTime();
        calendar.setTime(beginDate);
        calendar.add(Calendar.DAY_OF_YEAR, -30);
        beginDate = calendar.getTime();
        inOutRequest(Config.sdf3.format(beginDate),Config.sdf3.format(endDate));
    }

    private void updateIntervalTxt(){
        String[] str = Config.sdf.format(beginDate).split("-");
        String begin = str[0]+" "+str[1]+" "+str[2];
        str = Config.sdf.format(endDate).split("-");
        String end = str[0]+" "+str[1]+" "+str[2];
        end = begin+" - "+end;
        txtInterval.setText(end);
    }

    @Override
    public void refreshList() {
        endDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DAY_OF_YEAR, -30);
        beginDate = calendar.getTime();
        inOutRequest(Config.sdf3.format(beginDate),Config.sdf3.format(endDate));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        refreshTimer();
        if (getInOut!=null && !getInOut.isCancelled())
            getInOut.cancel(true);
    }

    @Override
    public void refreshTimer() {
        listener.refreshTimer();
    }

    @Override
    public void onItemClick(InOut inOut) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        refreshTimer();
        EnterExcuseFragment exFragment = new EnterExcuseFragment();
        Bundle bundle = new Bundle();
        if (inOut.getExcuse()!=null && !inOut.getExcuse().equals("")) {
            bundle.putString(BUNDLE_EXCUSE,inOut.getExcuse());
            exFragment.setArguments(bundle);
        }else exFragment.setCancelable(false);
        bundle.putString(BUNDLE_XID,inOut.getId());
        exFragment.setArguments(bundle);
        exFragment.setTargetFragment(this,0);
        exFragment.show(getFragmentManager(),"enter_ex");
    }

}
