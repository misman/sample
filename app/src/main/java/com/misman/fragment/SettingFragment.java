package com.misman.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.misman.listeners.LoginListener;
import com.misman.model.Config;
import com.misman.staffoperations.LoginActivity;
import com.misman.staffoperations.R;

/**
 * Created by misman on 08.06.2015.
 */
public class SettingFragment extends DialogFragment {

    public static final String MODE = "mode";
    public static final String LOGIN = "login";

    private EditText editPass,editIP;
    private LoginListener listener;
    private Handler handleTime;
    private Runnable runTime;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        Bundle args = getArguments();
        String mode = args.getString(MODE,"login");
        listener = (LoginListener) getActivity();
        handleTime = new Handler();
        runTime = new Runnable() {
            @Override
            public void run() {
                dismiss();
            }
        };
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view;

        if(mode.equals(LOGIN)){
            builder.setTitle("Enter Password");
            view = inflater.inflate(R.layout.fragment_login,null);
            editPass = (EditText) view.findViewById(R.id.setting_password);
            builder.setPositiveButton(R.string.general_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String password = editPass.getText().toString();
                    if (password.equals("123321")) {
                        listener.setting();
                    }
                    dismiss();
                }
            });
            handleTime.postDelayed(runTime,40*1000);
        }else{
            view = inflater.inflate(R.layout.fragment_setting, null);
            final SharedPreferences preferences = getActivity()
                    .getSharedPreferences(Config.PREF_FILE_NAME, Context.MODE_PRIVATE);
            Button closeApp = (Button) view.findViewById(R.id.btn_close);
            Button setExtra = (Button) view.findViewById(R.id.btn_set_extra);
            Button setDeve = (Button) view.findViewById(R.id.btn_set_develop);
            Button setCustom = (Button) view.findViewById(R.id.btn_set_custom);
            closeApp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });
            setDeve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editIP.getText().clear();
                    editIP.getText().append("http://development.misman.com.tr/rest/stuff/");
                }
            });
            setExtra.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editIP.getText().clear();
                    editIP.getText().append("http://extranet.misman.com.tr/rest/stuff/");
                }
            });
            setCustom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editIP.getText().clear();
                    editIP.getText().append("http://192.168.1.1:8080/TestRest/stuff/");
                }
            });
            editIP = (EditText) view.findViewById(R.id.setting_ip);
            String baseUrl = preferences.getString(Config.PREF_SERVICE_URL, Config.DEFAULT_BASE);
            editIP.getText().append(baseUrl);
            editIP.addTextChangedListener(watcher);

            builder.setPositiveButton(R.string.general_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String base_url = editIP.getText().toString();
                    if (!base_url.substring(base_url.length() - 1).equals("/"))
                        base_url+="/";
                    LoginActivity.BASE_URL = base_url;
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Config.PREF_SERVICE_URL, base_url);
                    editor.apply();
                }
            });
            handleTime.postDelayed(runTime,45*1000);
        }

        builder.setNegativeButton(R.string.general_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });
        builder.setView(view);
        return builder.create();
    }

    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            handleTime.removeCallbacksAndMessages(null);
            handleTime.postDelayed(runTime,45*1000);
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        handleTime.removeCallbacksAndMessages(null);
    }
}
