package com.misman.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.misman.model.Config;
import com.misman.model.Cryptor;
import com.misman.model.PersonInfo;
import com.misman.staffoperations.LoginActivity;
import com.misman.staffoperations.OpdetailActivity;
import com.misman.staffoperations.OperationsActivity;
import com.misman.staffoperations.R;
import com.misman.tool.Http;
import com.misman.tool.HttpRes;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

/**
 * Created by misman on 22.3.2016.
 */
public class PersonelInfo extends Fragment {

    private AsyncTask<String,String,Bitmap> getImage;
    private AsyncTask<String,String,HttpRes> getPersInfo;
    private ImageView imgPersonel;
    private CircularProgressBar progBarImg,progBarInfo;
    private RelativeLayout relHolder,headHolder;
    private TextView txtName, txtTC;
    private TextView txtIDCity, txtIDTown, txtIDDistrict, txtIDVillage, txtIDCilt
            , txtIDSayfa, txtIDSira, txtBornCity, txtMotherName, txtFatherName
            , txtBloodGroup, txtMobPhone, txtHomePhone;
    private TextView txtEmail,txtAddress, txtCompany, txtDepartment, txtManager
            , txtDateEmployed, txtProfession, txtCollarStat;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personel_info, container, false);
        imgPersonel = (ImageView) view.findViewById(R.id.img_person);
        relHolder = (RelativeLayout) view.findViewById(R.id.holder_info);
        headHolder = (RelativeLayout) view.findViewById(R.id.head_holder);
        progBarImg = (CircularProgressBar) view.findViewById(R.id.progress_bar);
        progBarInfo = (CircularProgressBar) view.findViewById(R.id.progress_bar_2);
        txtName = (TextView) view.findViewById(R.id.txt_person_name);
        txtTC = (TextView) view.findViewById(R.id.txt_person_tc);
        txtIDCity = (TextView) view.findViewById(R.id.txt_id_city);
        txtIDTown = (TextView) view.findViewById(R.id.txt_id_town);
        txtIDDistrict = (TextView) view.findViewById(R.id.txt_id_district);
        txtIDVillage = (TextView) view.findViewById(R.id.txt_id_village);
        txtIDCilt = (TextView) view.findViewById(R.id.txt_id_cilt);
        txtIDSayfa = (TextView) view.findViewById(R.id.txt_id_sayfa);
        txtIDSira = (TextView) view.findViewById(R.id.txt_id_sira);
        txtBornCity = (TextView) view.findViewById(R.id.txt_birth_place);
        txtMotherName = (TextView) view.findViewById(R.id.txt_mother_name);
        txtFatherName = (TextView) view.findViewById(R.id.txt_father_name);
        txtBloodGroup = (TextView) view.findViewById(R.id.txt_blood_type);
        txtMobPhone = (TextView) view.findViewById(R.id.txt_mob_phone);
        txtHomePhone = (TextView) view.findViewById(R.id.txt_home_phone);
        txtEmail = (TextView) view.findViewById(R.id.txt_email);
        txtAddress = (TextView) view.findViewById(R.id.txt_address);
        txtCompany = (TextView) view.findViewById(R.id.txt_company);
        txtDepartment = (TextView) view.findViewById(R.id.txt_department);
        txtManager = (TextView) view.findViewById(R.id.txt_manager);
        txtDateEmployed = (TextView) view.findViewById(R.id.txt_employed_date);
        txtProfession = (TextView) view.findViewById(R.id.txt_profession);
        txtCollarStat = (TextView) view.findViewById(R.id.txt_collar_stat);

        getImage = new AsyncTask<String, String, Bitmap>() {
            @Override
            protected Bitmap doInBackground(String... params) {
                Http http = new Http(LoginActivity.BASE_URL+
                        OperationsActivity.personel.Pers_Code+"/image"
                        , Http.RQ.GET);
                HttpRes res = http.getContentAsStream();
                if (res!=null){
                    if (res.getResponse()==200) {
                        return BitmapFactory.decodeStream(res.getContentStream());
                    }
                }

                return null;

            }

            @Override
            protected void onPostExecute(Bitmap res) {
                super.onPostExecute(res);
                if (res!=null){
                    imgPersonel.setImageBitmap(res);
                    progBarImg.setVisibility(View.INVISIBLE);
                    headHolder.setVisibility(View.VISIBLE);
                }else{
                    imgPersonel.setImageResource(R.mipmap.ic_worker);
                    progBarImg.setVisibility(View.INVISIBLE);
                    headHolder.setVisibility(View.VISIBLE);
                }
            }
        };

        getPersInfo = new AsyncTask<String, String, HttpRes>() {
            @Override
            protected HttpRes doInBackground(String... params) {
                Http http = new Http(LoginActivity.BASE_URL+
                        OperationsActivity.personel.Pers_Code+"/info"
                        , Http.RQ.GET);
                return http.startReq();
            }

            @Override
            protected void onPostExecute(HttpRes res) {
                super.onPostExecute(res);
                if (res!=null){
                    if (res.getResponse()==200){
                        String result = Cryptor.decrypt(res.getData());
                        PersonInfo info = OpdetailActivity.gson.fromJson(result,PersonInfo.class);
                        setInfo(info);
                        progBarInfo.setVisibility(View.INVISIBLE);
                        relHolder.setVisibility(View.VISIBLE);
                    }
                }
            }
        };

        return view;
    }


    private void setInfo(PersonInfo info){
        txtName.setText(info.name);
        txtTC.setText(info.tcNo);
        txtIDCity.setText(info.idCity);
        txtIDTown.setText(info.idTown);
        txtIDDistrict.setText(info.idDistrict);
        txtIDVillage.setText(info.idVillage);
        txtIDCilt.setText(info.idCilt);
        txtIDSayfa.setText(info.idSayfa);
        txtIDSira.setText(info.idSira);
        txtBornCity.setText(info.bornCity);
        txtMotherName.setText(info.motherName);
        txtFatherName.setText(info.fatherName);
        txtBloodGroup.setText(info.bloodGroup);
        txtMobPhone.setText(info.mobPhone);
        txtHomePhone.setText(info.homePhone);
        txtEmail.setText(info.email);
        String addresStr = info.address;
        if (info.city!=null)
            addresStr+="\t"+info.city;
        if (info.town!=null)
            addresStr+="-"+info.town;
        if (info.postCode!=null)
            addresStr+="\t"+info.postCode;
        if (info.country!=null)
            addresStr+="\t"+info.country;
        txtAddress.setText(addresStr);
        txtCompany.setText(info.company);
        txtDepartment.setText(info.department);
        txtManager.setText(info.manager);
        String dateEmpStr = Config.sdf2.format(info.dateEmployed);
        txtDateEmployed.setText(dateEmpStr.substring(0,dateEmpStr.length()-5));
        txtProfession.setText(info.profession);
        txtCollarStat.setText(info.collarStatus);
    }


    @Override
    public void onStart() {
        super.onStart();
        getImage.execute();
        getPersInfo.execute();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getImage!=null && !getImage.isCancelled())
            getImage.cancel(true);
        if (getPersInfo!=null && !getPersInfo.isCancelled())
            getPersInfo.cancel(true);
    }
}
