package com.misman.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.misman.listeners.InOutListener;
import com.misman.model.Cryptor;
import com.misman.model.InOut;
import com.misman.staffoperations.LoginActivity;
import com.misman.staffoperations.OpdetailActivity;
import com.misman.staffoperations.R;
import com.misman.tool.Http;
import com.misman.tool.HttpRes;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

/**
 * Created by misman on 08.06.2015.
 */
public class EnterExcuseFragment extends DialogFragment {

    private EditText editExcuse;
    private InOutListener listener;
    private String xId;
    private SendExcuse sendEx;
    private CircularProgressBar cpBar;
    private RelativeLayout hold;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        listener = (InOutListener) getTargetFragment();
        Bundle args = getArguments();
        String data = args.getString(InOutEntry.BUNDLE_EXCUSE);
        xId = args.getString(InOutEntry.BUNDLE_XID);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_enter_excuse, null);
        editExcuse = (EditText) view.findViewById(R.id.edit_excuse);
        TextView txtExcuse = (TextView) view.findViewById(R.id.txt_excuse);
        Button btnSend = (Button) view.findViewById(R.id.btn_send);
        Button btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        if (data!=null){
            builder.setTitle(R.string.title_excuse);
            txtExcuse.setVisibility(View.VISIBLE);
            editExcuse.setVisibility(View.GONE);
            editExcuse.addTextChangedListener(watcher);
            txtExcuse.setText(data);
            btnCancel.setVisibility(View.GONE);
            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.refreshTimer();
                    dismiss();
                }
            });
        }else {
            hold = (RelativeLayout) view.findViewById(R.id.hold);
            cpBar = (CircularProgressBar) view.findViewById(R.id.progress_bar);
            builder.setTitle(R.string.title_excuse_enter);
            txtExcuse.setVisibility(View.GONE);
            editExcuse.setVisibility(View.VISIBLE);
            editExcuse.setHorizontallyScrolling(false);
            editExcuse.setLines(3);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.refreshTimer();
                    dismiss();
                }
            });
            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.refreshTimer();
                    Editable excusable = editExcuse.getText();
                    if (!TextUtils.isEmpty(excusable)){
                        listener.refreshTimer();
                        sendEx = new SendExcuse();
                        sendEx.execute(excusable.toString());
                    }
                }
            });
        }
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (sendEx!=null && !sendEx.isCancelled())
            sendEx.cancel(true);
    }

    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            listener.refreshTimer();
        }
    };

    private class SendExcuse extends AsyncTask<String,String,HttpRes>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            hold.setVisibility(View.INVISIBLE);
            cpBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected HttpRes doInBackground(String... params) {
            Http http = new Http(LoginActivity.BASE_URL+"writeexcuse", Http.RQ.POST);
            InOut inOut = new InOut(xId,params[0]);
            String data = OpdetailActivity.gson.toJson(inOut);
            http.setDataSend(Cryptor.encrypt(data));
            return http.startReq();
        }

        @Override
        protected void onPostExecute(HttpRes httpRes) {
            super.onPostExecute(httpRes);
            if (httpRes!=null){
                if (httpRes.getResponse()==200){
                    listener.refreshTimer();
                    listener.refreshList();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dismiss();
                        }
                    },1000);
                    return;
                }else Toast.makeText(getActivity(), "Servis işlemi hatası", Toast.LENGTH_SHORT).show();
            }else Toast.makeText(getActivity(), "Servis Erişim Hatası", Toast.LENGTH_SHORT).show();
            hold.setVisibility(View.VISIBLE);
            cpBar.setVisibility(View.GONE);
        }
    }

}
