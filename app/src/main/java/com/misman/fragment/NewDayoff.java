package com.misman.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.misman.listeners.DayOffListener;
import com.misman.listeners.DetailListener;
import com.misman.listeners.TimePickListen;
import com.misman.model.Config;
import com.misman.model.Cryptor;
import com.misman.model.DayOff;
import com.misman.staffoperations.LoginActivity;
import com.misman.staffoperations.OperationsActivity;
import com.misman.staffoperations.R;
import com.misman.tool.Http;
import com.misman.tool.HttpRes;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Minutes;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by misman on 14.3.2016.
 */
public class NewDayoff extends DialogFragment implements TimePickListen {

    private RelativeLayout progressLay;
    private RelativeLayout holder;
    private TextView txtHire, txtRemain, txtManager,txtResult;
    private EditText editInfo;
    private AsyncTask<String,String,HttpRes> newDayTask,reqDayTask;
    private Gson gson;
    private Button btnStart,btnFinish;
    private DayOffListener listener;
    private DetailListener listenTimer;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        listenTimer = (DetailListener) getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.fragment_new_dayoff,null);
        listener = (DayOffListener) getTargetFragment();
        progressLay = (RelativeLayout) view.findViewById(R.id.lay_progress);
        holder = (RelativeLayout) view.findViewById(R.id.holder);
        txtHire = (TextView) view.findViewById(R.id.txt_person_hire);
        txtRemain = (TextView) view.findViewById(R.id.txt_remain_day);
        txtManager = (TextView) view.findViewById(R.id.txt_person_manager);
        txtResult = (TextView) view.findViewById(R.id.txt_result);
        editInfo = (EditText) view.findViewById(R.id.new_dayoff_info);
        editInfo.setHorizontallyScrolling(false);
        editInfo.setLines(4);
        editInfo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                listenTimer.refreshTimer();
            }
        });
        btnStart = (Button) view.findViewById(R.id.btn_start);
        btnFinish = (Button) view.findViewById(R.id.btn_finish);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listenTimer.refreshTimer();
                DatetimePick picker = DatetimePick.newInstance(false, null);
                picker.setCancelable(false);
                picker.setTargetFragment(NewDayoff.this, 0);
                picker.show(getChildFragmentManager(), "timepicker");
            }
        });
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listenTimer.refreshTimer();
                CharSequence startDateStr = btnStart.getText();
                if (!TextUtils.isEmpty(startDateStr)) {
                    DatetimePick picker = DatetimePick.newInstance(true, startDateStr.toString());
                    picker.setCancelable(false);
                    picker.setTargetFragment(NewDayoff.this, 0);
                    picker.show(getChildFragmentManager(), "timepicker");
                } else {
                    Toast.makeText(getActivity(), "Lütfen Önce Başlama Tarihini Seçiniz!", Toast.LENGTH_LONG).show();
                }
            }
        });

        Button cancel = (Button) view.findViewById(R.id.btn_cancel);
        Button send = (Button) view.findViewById(R.id.btn_send);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listenTimer.refreshTimer();
                dismiss();
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listenTimer.refreshTimer();
                CharSequence startAble = btnStart.getText();
                CharSequence finishAble = btnFinish.getText();
                CharSequence info = editInfo.getText();
                if (!TextUtils.isEmpty(startAble) && !TextUtils.isEmpty(finishAble)
                        && !TextUtils.isEmpty(info)){
                    try {
                        Date stDate = Config.sdf2.parse(startAble.toString());
                        Date fnDate = Config.sdf2.parse(finishAble.toString());
                        String rs = listener.checkDayoffList(stDate,fnDate);
                        if (rs.equals("0")) {
                            DayOff dayOff = new DayOff(stDate, fnDate, info.toString());
                            dayOff.setPersCode(OperationsActivity.personel.Pers_Code);
                            dayOff.setHrpType("F");
                            String encData = Cryptor.encrypt(gson.toJson(dayOff));
                            sendRequest(encData);
                        }else if(rs.equals("1")){
                            Toast.makeText(getActivity(),"Diğer İzin Talepleriniz İle Çakışmaktadır",
                                    Toast.LENGTH_LONG).show();
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }else {
                    Toast.makeText(getActivity(),"Zorunlu Alanları Boş Bırakmayınız"
                            ,Toast.LENGTH_SHORT).show();
                }
            }
        });
        TextView txtPersonName = (TextView) view.findViewById(R.id.txt_person_name);
        String personStr = OperationsActivity.personel.Pers_Code+" - "+
                OperationsActivity.personel.Pers_Name;
        txtPersonName.setText(personStr);
        gson = new GsonBuilder().disableHtmlEscaping().
                setDateFormat("ddMMyyyyHHmmss").create();
        newDayTask = new AsyncTask<String, String, HttpRes>() {
            @Override
            protected HttpRes doInBackground(String... params) {
                Http http = new Http(LoginActivity.BASE_URL+
                        OperationsActivity.personel.Pers_Code+"/restinfo", Http.RQ.GET);
                return http.startReq();
            }

            @Override
            protected void onPostExecute(HttpRes httpRes) {
                super.onPostExecute(httpRes);
                progressLay.setVisibility(View.INVISIBLE);
                holder.setVisibility(View.VISIBLE);
                if (httpRes!=null) {
                    if (httpRes.getResponse() == 200) {
                        String result = Cryptor.decrypt(httpRes.getData());
                        Config.DayoffInfo info = gson.fromJson(result,
                                Config.DayoffInfo.class);
                        String hDate = Config.sdf2.format(info.HireDate);
                        txtHire.setText(hDate.substring(0,hDate.length()-6));
                        txtRemain.setText(info.RestDay);
                        txtManager.setText(info.ManagerName);
                        return;
                    }
                }
                dismiss();
                Toast.makeText(getActivity(),"Servise Erişim Problemi"
                        ,Toast.LENGTH_SHORT).show();
            }
        };

        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.fragment_new_dayoff_width);
        int height = getResources().getDimensionPixelSize(R.dimen.fragment_new_dayoff_height);
        getDialog().getWindow().setLayout(width, height);
        newDayTask.execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (newDayTask!=null && !newDayTask.isCancelled())
            newDayTask.cancel(true);

        if (reqDayTask!=null && !reqDayTask.isCancelled())
            reqDayTask.cancel(true);
    }

    private void sendRequest(String data){
        reqDayTask = new AsyncTask<String, String, HttpRes>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                InputMethodManager imm = (InputMethodManager) getActivity()
                        .getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editInfo.getWindowToken(),0);
                progressLay.setVisibility(View.VISIBLE);
                holder.setVisibility(View.INVISIBLE);
            }

            @Override
            protected HttpRes doInBackground(String... params) {
                Http http = new Http(LoginActivity.BASE_URL + "createrest" , Http.RQ.POST);
                http.setDataSend(params[0]);
                return http.startReq();
            }

            @Override
            protected void onPostExecute(HttpRes httpRes) {
                super.onPostExecute(httpRes);
                progressLay.setVisibility(View.INVISIBLE);
                if (httpRes!=null){
                    if (httpRes.getResponse()==200){
                        txtResult.setVisibility(View.VISIBLE);
                        txtResult.setText("TALEBİNİZ YÖNETİCİNİZE İLETİLMİŞTİR");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                listener.refreshList();
                                dismiss();
                            }
                        },2000);
                    }else if(httpRes.getResponse()==201){
                        holder.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(),"Bu Tarihli Kayıtlı İzin Talebiniz Vardır. Lütfen Farklı Bir Tarih Giriniz!"
                                ,Toast.LENGTH_LONG).show();
                    }else {
                        holder.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(),"Service Ekleme Yapamadı"
                                ,Toast.LENGTH_SHORT).show();
                    }
                }else {
                    holder.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(),"Servise Erişim Problemi"
                            ,Toast.LENGTH_SHORT).show();
                }
            }
        };
        reqDayTask.execute(data);
    }

    @Override
    public void setStartTime(Date start) {
        listenTimer.refreshTimer();
        CharSequence finishAble = btnFinish.getText();
        if (!TextUtils.isEmpty(finishAble)){
            try {
                Date finish = Config.sdf2.parse(finishAble.toString());
                if (!start.before(finish)){
                    btnFinish.setText("");
                    btnFinish.setHint(R.string.finish_date);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
        btnStart.setText(Config.sdf2.format(start));
    }

    @Override
    public void setFinishTime(Date finish) {
        listenTimer.refreshTimer();
        try {
            Date start = Config.sdf2.parse(btnStart.getText().toString());
            DateTime stDtTime = new DateTime(start);
            DateTime fnDtTime = new DateTime(finish);
            int countMinutes = Minutes.minutesBetween(stDtTime,fnDtTime).getMinutes();
            int countDays = Days.daysBetween(stDtTime,fnDtTime).getDays();
            if (countMinutes<2 || countDays>30){
                btnFinish.setText("");
                Toast.makeText(getActivity(), " Bu Süreli İzin Talebi Oluşturulamaz!"
                        , Toast.LENGTH_LONG).show();
                return;
            }

            if (finish.after(start)) {
                btnFinish.setText(Config.sdf2.format(finish));
            } else {
                Toast.makeText(getActivity(), "Bitiş Zamanı Başlangıç Zamanından" +
                        " Önce Olamaz", Toast.LENGTH_SHORT).show();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
