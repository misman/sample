package com.misman.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import com.misman.listeners.DatePickListen;
import com.misman.model.Config;
import com.misman.staffoperations.R;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by misman on 24.06.2015.
 */
public class DatePick extends DialogFragment {

    public static final String BUNDLE_DATE = "date";
    private Date startDate;
    private DatePickListen listen;

    public static DatePick newInstance(String startDate){
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_DATE,startDate);
        DatePick datetimePick = new DatePick();
        datetimePick.setArguments(bundle);
        return datetimePick;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listen = (DatePickListen) getTargetFragment();
        Bundle bundle = getArguments();
        try {
            startDate = Config.sdf5.parse(bundle.getString(BUNDLE_DATE));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.date, null);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.date_picker);

        int year;
        int month;
        int day;

        Calendar inspectedAt = Calendar.getInstance();
        inspectedAt.setTime(startDate);
        year    = inspectedAt.get(Calendar.YEAR) ;
        month   = inspectedAt.get(Calendar.MONTH);
        day     = inspectedAt.get(Calendar.DAY_OF_MONTH);
        datePicker.updateDate(year, month, day);

        builder.setPositiveButton(R.string.general_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                int selYear = datePicker.getYear();
                int selMounth = datePicker.getMonth();
                int selDay = datePicker.getDayOfMonth();

                Calendar selCal = Calendar.getInstance();
                selCal.set(selYear, selMounth, selDay);
                listen.setDate(selCal.getTime());
            }
        });

        builder.setNegativeButton(R.string.general_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });
        builder.setView(view);
        final AlertDialog alert = builder.create();
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                int height = getResources().getDimensionPixelSize(R.dimen.frag_date_btn_height);
                int fontSize = getResources().getDimensionPixelSize(R.dimen.frag_btn_font_size);
                Button btnPositive = alert.getButton(Dialog.BUTTON_POSITIVE);
                btnPositive.setHeight(height);
                btnPositive.setTextSize(fontSize);

                Button btnNegative = alert.getButton(Dialog.BUTTON_NEGATIVE);
                btnNegative.setHeight(height);
                btnNegative.setTextSize(fontSize);
            }
        });

        return alert;
    }

}
