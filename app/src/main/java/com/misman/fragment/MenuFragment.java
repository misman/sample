package com.misman.fragment;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.misman.listeners.MenuListener;
import com.misman.staffoperations.R;
import com.misman.tool.RecyclerAdapter;

/**
 * Created by misman on 9.3.2016.
 */
public class MenuFragment extends Fragment implements RecyclerAdapter.RecyclerClick {

    private MenuListener listener;
    public static final String BUNDLE_SUB_MENU="sub_menu";
    private int subMenuId;
    private long mLastClickTime;
    String[] data=null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list_menu_2);
        listener = (MenuListener) getActivity();
        String[] menu = getResources().getStringArray(R.array.menu_1);
        RecyclerAdapter adapter;
        if (getArguments()==null){
            subMenuId=-1;
            adapter = new RecyclerAdapter(this,menu);
        }else {
            subMenuId = getArguments().getInt(BUNDLE_SUB_MENU);
            switch (subMenuId){
                case 0:data = getResources().getStringArray(R.array.menu_2_1);
                    break;
                case 1:data = getResources().getStringArray(R.array.menu_2_2);
                    break;
            }
            listener.setTitle(menu[subMenuId]);
            adapter = new RecyclerAdapter(this,data);
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onItemClick(int position) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        if (subMenuId==-1){
            listener.turn(position);
        }else {
            String code = String.valueOf(subMenuId)+String.valueOf(position);
            listener.fromSubMenu(code,data[position]);
        }
    }
}
