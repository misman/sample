package com.misman.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.misman.listeners.DayOffListener;
import com.misman.listeners.DetailListener;
import com.misman.model.Cryptor;
import com.misman.model.DayOff;
import com.misman.staffoperations.LoginActivity;
import com.misman.staffoperations.OpdetailActivity;
import com.misman.staffoperations.OperationsActivity;
import com.misman.staffoperations.R;
import com.misman.tool.DayOffAdapter;
import com.misman.tool.Http;
import com.misman.tool.HttpRes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import at.markushi.ui.CircleButton;

/**
 * Created by misman on 11.3.2016.
 */
public class DayOffDemand extends Fragment implements DayOffListener {

    private AsyncTask<String,String,HttpRes> getDayOffTask;
    private RecyclerView listDayOff;
    private long mLastClickTime=0;
    private DetailListener listener;
    private ArrayList<DayOff> dayOffs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dayoff_demand,container,false);
        listener = (DetailListener) getActivity();
        CircleButton btnNew = (CircleButton) view.findViewById(R.id.img_new);
        btnNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.refreshTimer();
                NewDayoff newDayoff = new NewDayoff();
                newDayoff.setCancelable(false);
                newDayoff.setTargetFragment(DayOffDemand.this,0);
                newDayoff.show(getChildFragmentManager(),"new_day_off");
            }
        });
        listDayOff = (RecyclerView) view.findViewById(R.id.list_dayoff_demand);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        refreshList();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getDayOffTask!=null && !getDayOffTask.isCancelled())
            getDayOffTask.cancel(true);
    }

    @Override
    public void onItemClick(int position) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
    }

    @Override
    public String checkDayoffList(Date start, Date finish){
        if (dayOffs!=null && !dayOffs.isEmpty()) {
            for (DayOff dayOff : dayOffs) {
                if (dayOff.checkConflict(start, finish)) {
                    return "1";
                }
            }
        }
        return "0";
    }

    @Override
    public void refreshList() {
        getDayOffTask = new AsyncTask<String, String, HttpRes>() {
            @Override
            protected HttpRes doInBackground(String... params) {
                String urlMake = LoginActivity.BASE_URL+
                        OperationsActivity.personel.Pers_Code+"/F/restlist";
                Http http = new Http(urlMake, Http.RQ.GET);
                return http.startReq();
            }

            @Override
            protected void onPostExecute(HttpRes httpRes) {
                super.onPostExecute(httpRes);
                if (httpRes!=null) {
                    String jsonDayOffs = Cryptor.decrypt(httpRes.getData());
                    if (httpRes.getResponse() == 200) {
                        dayOffs = OpdetailActivity.gson.fromJson(jsonDayOffs,
                                new TypeToken<ArrayList<DayOff>>(){}.getType());
                        if (dayOffs!=null && !dayOffs.isEmpty()){
                            Collections.sort(dayOffs);
                            listDayOff.setLayoutManager(new LinearLayoutManager(getActivity()));
                            listDayOff.setAdapter(new DayOffAdapter(DayOffDemand.this,dayOffs));
                        }else {
                            Toast.makeText(getActivity(),"İzin Bilgisi Bulunmamaktadır",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(getActivity(),"Sunucuda hata oluştu",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };
        getDayOffTask.execute();
    }
}
