package com.misman.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.misman.listeners.DatePickListen;
import com.misman.listeners.DetailListener;
import com.misman.model.Config;
import com.misman.model.Cryptor;
import com.misman.model.InOutDaily;
import com.misman.staffoperations.LoginActivity;
import com.misman.staffoperations.OpdetailActivity;
import com.misman.staffoperations.OperationsActivity;
import com.misman.staffoperations.R;
import com.misman.tool.DailyInOutAdapter;
import com.misman.tool.Http;
import com.misman.tool.HttpRes;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

/**
 * Created by misman on 4.04.2016.
 */
public class InOutDailyFrag extends Fragment implements DatePickListen {

    private AsyncTask<String,String,HttpRes> getInOut;
    private Date currentDate;
    private DetailListener listener;
    private ListView listView;
    private CircularProgressBar progBar;
    private TextView txtInterval;
    private long mLastClickTime;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inout_daily,container,false);
        RelativeLayout next = (RelativeLayout) view.findViewById(R.id.btn_next_month);
        RelativeLayout previous = (RelativeLayout) view.findViewById(R.id.btn_pre_month);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 600) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                nextDay();
                listener.refreshTimer();
            }
        });
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 600) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                preDay();
                listener.refreshTimer();
            }
        });
        txtInterval = (TextView) view.findViewById(R.id.txt_interval);
        txtInterval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.refreshTimer();
                DatePick datePick = DatePick.newInstance(Config.sdf5.format(currentDate));
                datePick.setCancelable(false);
                datePick.setTargetFragment(InOutDailyFrag.this,0);
                datePick.show(getChildFragmentManager(),"date_pick");
            }
        });
        listView = (ListView) view.findViewById(R.id.list_in_out);
        progBar = (CircularProgressBar) view.findViewById(R.id.progress_bar);
        listener = (DetailListener) getActivity();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        currentDate = new Date();
        inOutRequest(Config.sdf3.format(currentDate));
    }

    private void inOutRequest(String date){
        getInOut = new AsyncTask<String, String, HttpRes>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                updateIntervalTxt();
                progBar.setVisibility(View.VISIBLE);
                listView.setVisibility(View.INVISIBLE);
            }

            @Override
            protected HttpRes doInBackground(String... params) {
                String url = LoginActivity.BASE_URL + OperationsActivity.personel.Pers_Code
                        + "/" + params[0] + "/dailypdkslist";
                Http http = new Http(url, Http.RQ.GET);
                HttpRes res = http.startReq();
                if (res!=null){
                    res.setData(Cryptor.decrypt(res.getData()));
                }
                return res;
            }

            @Override
            protected void onPostExecute(HttpRes httpRes) {
                super.onPostExecute(httpRes);
                if (httpRes!=null){
                    if (httpRes.getResponse()==200){
                        ArrayList<InOutDaily> inOuts = OpdetailActivity.gson.
                                fromJson(httpRes.getData(),
                                        new TypeToken<ArrayList<InOutDaily>>(){}.getType());
                        listView.setAdapter(new DailyInOutAdapter(getActivity(), inOuts));
                        progBar.setVisibility(View.INVISIBLE);
                        listView.setVisibility(View.VISIBLE);
                    }else {
                        Toast.makeText(getActivity(),"Veri Problemi",
                                Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(),"Servis Erişim Problemi",
                            Toast.LENGTH_SHORT).show();
                }
            }
        }.execute(date);
    }

    private void nextDay(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        currentDate = calendar.getTime();
        inOutRequest(Config.sdf3.format(currentDate));
    }

    private void preDay(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        currentDate = calendar.getTime();
        inOutRequest(Config.sdf3.format(currentDate));
    }

    private void updateIntervalTxt(){
        String[] str1 = Config.sdf4.format(currentDate).split("-");
        String[] str = Config.sdf5.format(currentDate).split("-");
        String result = str[0] +", " + str1[1];
        txtInterval.setText(result);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        listener.refreshTimer();
        if (getInOut!=null && !getInOut.isCancelled())
            getInOut.cancel(true);
    }

    @Override
    public void setDate(Date date) {
        currentDate = date;
        inOutRequest(Config.sdf3.format(currentDate));
        listener.refreshTimer();
    }
}
