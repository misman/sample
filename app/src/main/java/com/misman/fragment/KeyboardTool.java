package com.misman.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.misman.listeners.LoginListener;
import com.misman.staffoperations.R;

public class KeyboardTool extends Fragment {

    private Button btnDel,btnEnter;

	private static EditText editIndicator;
	private static Editable editableIndicator;
	private LoginListener listener;

	public KeyboardTool() {

	}

    @Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		listener = (LoginListener) getActivity();
        View rootView = inflater.inflate(R.layout.fragment_keyboard, container, false);
        InitElements(rootView);
		return rootView;
	}

    private void InitElements(View rootView) {
		Button btnOne = (Button) rootView.findViewById(R.id.btnNu1);
		Button btnTwo = (Button) rootView.findViewById(R.id.btnNu2);
		Button btnThree = (Button) rootView.findViewById(R.id.btnNu3);
		Button btnFour = (Button) rootView.findViewById(R.id.btnNu4);
		Button btnFive = (Button) rootView.findViewById(R.id.btnNu5);
		Button btnSix = (Button) rootView.findViewById(R.id.btnNu6);
		Button btnSeven = (Button) rootView.findViewById(R.id.btnNu7);
		Button btnEight = (Button) rootView.findViewById(R.id.btnNu8);
		Button btnNine = (Button) rootView.findViewById(R.id.btnNu9);
		Button btnZero = (Button) rootView.findViewById(R.id.btnNu0);
        btnDel = (Button) rootView.findViewById(R.id.btnDel);
        btnEnter = (Button) rootView.findViewById(R.id.btnEnter);

		btnOne.setOnClickListener(btnNumberClickListener);
		btnTwo.setOnClickListener(btnNumberClickListener);
		btnThree.setOnClickListener(btnNumberClickListener);
		btnFour.setOnClickListener(btnNumberClickListener);
		btnFive.setOnClickListener(btnNumberClickListener);
		btnSix.setOnClickListener(btnNumberClickListener);
		btnSeven.setOnClickListener(btnNumberClickListener);
		btnEight.setOnClickListener(btnNumberClickListener);
		btnNine.setOnClickListener(btnNumberClickListener);
		btnZero.setOnClickListener(btnNumberClickListener);
		btnEnter.setOnClickListener(btnNumberClickListener);
		btnDel.setOnClickListener(btnNumberClickListener);
	}



   	private OnClickListener btnNumberClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
            v.playSoundEffect(SoundEffectConstants.CLICK);
			Button clickedBtn = (Button)v;
		    String btnText = clickedBtn.getText().toString();

            int start = editIndicator.getSelectionStart();

            if(clickedBtn.getId()==btnEnter.getId()){
				if (!TextUtils.isEmpty(editableIndicator)) {
					listener.enter(editableIndicator.toString());
				}
		    }else if(clickedBtn.getId()==btnDel.getId()){
                if( editableIndicator!=null && start>0 ){
                    editableIndicator.delete(start - 1, start);
                }
            }else{
		    	 editableIndicator.insert(start, btnText);
		    }
			
		}
	};

    public void changeFocus(EditText editText){
        editIndicator = editText;
        editableIndicator = editText.getText();
    }
}
