package com.misman.listeners;

import java.util.Date;

/**
 * Created by misman on 22.2.2016.
 */
public interface TimePickListen {
    void setStartTime(Date start);
    void setFinishTime(Date finish);
}
