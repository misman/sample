package com.misman.listeners;

/**
 * Created by misman on 9.3.2016.
 */
public interface MenuListener {
    void turn(int position);
    void fromSubMenu(String code, String title);
    void setTitle(String text);
}
