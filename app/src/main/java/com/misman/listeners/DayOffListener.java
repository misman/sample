package com.misman.listeners;

import java.util.Date;

/**
 * Created by misman on 16.3.2016.
 */
public interface DayOffListener {
    String checkDayoffList(Date start, Date finish);
    void onItemClick(int position);
    void refreshList();
}
