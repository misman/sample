package com.misman.listeners;

import java.util.Date;

/**
 * Created by misman on 5.04.2016.
 */
public interface DatePickListen {
    void setDate(Date date);
}
