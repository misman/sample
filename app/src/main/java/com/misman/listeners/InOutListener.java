package com.misman.listeners;

import com.misman.model.InOut;

/**
 * Created by misman on 30.03.2016.
 */
public interface InOutListener {
    void refreshTimer();
    void refreshList();
    void onItemClick(InOut inOut);
}
