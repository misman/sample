package com.misman.staffoperations;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.misman.fragment.KeyboardTool;
import com.misman.fragment.ProgressFragment;
import com.misman.fragment.SettingFragment;
import com.misman.listeners.LoginListener;
import com.misman.model.Config;
import com.misman.model.Cryptor;
import com.misman.tool.CustomView;
import com.misman.tool.Http;
import com.misman.tool.HttpRes;

import net.danlew.android.joda.JodaTimeAndroid;

public class LoginActivity extends AppCompatActivity implements LoginListener {

    public static final String INTENT_PERSONEL = "personel";
    public static String BASE_URL;
    private AsyncTask<String,String,HttpRes> loginTask;
    private EditText editTC;
    private KeyboardTool keyboardTool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        WindowManager manager = ((WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE));
        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
        localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        localLayoutParams.gravity = Gravity.TOP;
        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|

                // this is to enable the notification to recieve touch events
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |

                // Draws over status bar
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        localLayoutParams.height = (int) (50 * getResources()
                .getDisplayMetrics().scaledDensity);
        localLayoutParams.format = PixelFormat.TRANSPARENT;

        CustomView view = new CustomView(this);
        manager.addView(view, localLayoutParams);

        ImageView logo = (ImageView) findViewById(R.id.logo_img);
        logo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                SettingFragment sf = new SettingFragment();
                Bundle bundle = new Bundle();
                bundle.putString(SettingFragment.MODE, SettingFragment.LOGIN);
                sf.setArguments(bundle);
                sf.show(getSupportFragmentManager(), "login");
                return false;
            }
        });
        JodaTimeAndroid.init(this);
        SharedPreferences pref = getSharedPreferences(
                Config.PREF_FILE_NAME, MODE_PRIVATE);
        BASE_URL = pref.getString(Config.PREF_SERVICE_URL,
                Config.DEFAULT_BASE);
        editTC = (EditText) findViewById(R.id.edit_tc);
        keyboardTool = new KeyboardTool();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.keyboard_holder, keyboardTool, "keyboard");
        ft.commit();

    }

    @Override
    public void enter(String tcNo) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment keyboard = fm.findFragmentByTag("keyboard");
        ft.hide(keyboard);
        ft.add(R.id.keyboard_holder, new ProgressFragment(), "progress");
        ft.commit();

        loginTask = new AsyncTask<String,String,HttpRes>() {
            @Override
            protected HttpRes doInBackground(String[] params) {
                Http http = new Http(BASE_URL+params[0], Http.RQ.GET);
                return http.startReq();
            }

            @Override
            protected void onPostExecute(HttpRes response) {
                super.onPostExecute(response);
                if (response!=null) {
                    if (response.getResponse() == 200) {
                        editTC.getText().clear();
                        Intent intent = new Intent(LoginActivity.this, OperationsActivity.class);
                        intent.putExtra(INTENT_PERSONEL, Cryptor.decrypt(response.getData()));
                        startActivity(intent);
                    } else {
                        Toast.makeText(LoginActivity.this, "Tekrar Deneyiniz",
                                Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(LoginActivity.this, "Servise Erişim Hatası",
                            Toast.LENGTH_SHORT).show();
                }
                changeToKeyboard();
            }
        };
        loginTask.execute(tcNo);
    }

    @Override
    public void setting() {
        SettingFragment sf = new SettingFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SettingFragment.MODE,"setting");
        sf.setArguments(bundle);
        sf.show(getSupportFragmentManager(),"setting");
    }

    @Override
    protected void onResume() {
        super.onResume();
        keyboardTool.changeFocus(editTC);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (loginTask!=null && !loginTask.isCancelled())
          loginTask.cancel(true);
    }

    private void changeToKeyboard(){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment keyboard = fm.findFragmentByTag("keyboard");
        Fragment progress = fm.findFragmentByTag("progress");
        ft.show(keyboard);
        ft.remove(progress);
        ft.commit();
    }


}
