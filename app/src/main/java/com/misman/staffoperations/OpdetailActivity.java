package com.misman.staffoperations;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.misman.fragment.DayOffDemand;
import com.misman.fragment.DayoffUsed;
import com.misman.fragment.InOutDailyFrag;
import com.misman.fragment.InOutEntry;
import com.misman.fragment.PersonelInfo;
import com.misman.listeners.DetailListener;
import com.misman.model.Config;

/**
 * Created by misman on 11.3.2016.
 */
public class OpdetailActivity extends AppCompatActivity implements DetailListener {

    private Handler handle;
    private Runnable run;
    public static Gson gson;
    private TextView txtName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operation_detail);
        gson = new GsonBuilder().disableHtmlEscaping().
                setDateFormat("ddMMyyyyHHmmss").create();
        handle = new Handler();
        run = new Runnable() {
            @Override
            public void run() {
                setResult(Config.RESULT_TIMER);
                finish();
            }
        };
        String code = getIntent().getStringExtra(OperationsActivity.INTENT_FRAG_CODE);
        String title = getIntent().getStringExtra(OperationsActivity.INTENT_FRAG_TITLE);
        TextView txtExit = (TextView) findViewById(R.id.txt_exit);
        txtName = (TextView) findViewById(R.id.txt_bar_name);
        TextView txtBarTitle = (TextView) findViewById(R.id.txt_bar_title);
        txtBarTitle.setText(title);
        txtName.setText(OperationsActivity.personel.Pers_Name);

        txtExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(-1);
                finish();
            }
        });
        setFragment(code);
    }

    private void setFragment(String code) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        switch (code){
            case "00":
                txtName.setText("");
                ft.replace(R.id.detail_holder, new PersonelInfo(), "pers_info");
                break;
            case "01":
                ft.replace(R.id.detail_holder, new DayOffDemand(), "dayoff_demand");
                break;
            case "02":
                ft.replace(R.id.detail_holder, new DayoffUsed(), "dayoff_used");
                break;
            case "03":
                break;
            case "10":
                ft.replace(R.id.detail_holder, new InOutEntry(), "inout_pdks");
                break;
            case "11":
                ft.replace(R.id.detail_holder, new InOutDailyFrag(), "inout_daily_pdks");
                break;
            default:
                System.out.println("There is nothing like that");
                break;
        }
        ft.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        handle.postDelayed(run,Config.OPERATION_TIME);
    }

    @Override
    public void refreshTimer() {
        handle.removeCallbacksAndMessages(null);
        handle.postDelayed(run,Config.OPERATION_TIME);
    }

    @Override
    protected void onStop() {
        super.onStop();
        handle.removeCallbacksAndMessages(null);
    }
}
