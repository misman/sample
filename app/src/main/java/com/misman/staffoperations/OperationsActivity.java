package com.misman.staffoperations;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.misman.fragment.MenuFragment;
import com.misman.listeners.MenuListener;
import com.misman.model.Config;

import java.util.Locale;

/**
 * Created by misman on 9.3.2016.
 */
public class OperationsActivity extends AppCompatActivity implements MenuListener {

    public static final String INTENT_FRAG_CODE = "fr_code";
    public static final String INTENT_FRAG_TITLE = "fr_tit";
    private int mode=0;
    public static Config.Personel personel;
    private TextView txtTitle;
    private Handler handle;
    private Runnable run;
    private ImageView backImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operation);
        Locale.setDefault(new Locale("tr"));
        String jsonPersonel = getIntent().getStringExtra(LoginActivity.INTENT_PERSONEL);
        personel = new Gson().fromJson(jsonPersonel, Config.Personel.class);
        RelativeLayout backLayout = (RelativeLayout) findViewById(R.id.back_layout);
        backImage = (ImageView) findViewById(R.id.back_img);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mode==1){
                    backImage.setVisibility(View.GONE);
                    refreshTimer();
                    FragmentManager fm = getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.list_holder,
                            new MenuFragment(), "menu1").commit();
                    setTitle("İşlemler");
                    mode--;
                }
            }
        });
        handle = new Handler();
        run = new Runnable() {
            @Override
            public void run() {
                finish();
            }
        };
        TextView txtName = (TextView) findViewById(R.id.txt_bar_name);
        TextView txtExit = (TextView) findViewById(R.id.txt_exit);
        txtExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        txtTitle = (TextView) findViewById(R.id.txt_bar_title);
        txtTitle.setText("İşlemler");
        txtName.setText(personel.Pers_Name);
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.list_holder,
                new MenuFragment(), "menu1").commit();
        refreshTimer();
    }

    @Override
    public void setTitle(String text){
        txtTitle.setText(text);
    }

    public void refreshTimer() {
        handle.removeCallbacksAndMessages(null);
        handle.postDelayed(run, Config.OPERATION_TIME);
    }

    @Override
    public void turn(int position) {
        backImage.setVisibility(View.VISIBLE);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MenuFragment menuFragment = new MenuFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(MenuFragment.BUNDLE_SUB_MENU, position);
        menuFragment.setArguments(bundle);
        ft.replace(R.id.list_holder, menuFragment, "menu2").commit();
        refreshTimer();
        mode++;
    }

    @Override
    public void fromSubMenu(String code, String title) {
            handle.removeCallbacksAndMessages(null);
            Intent intent = new Intent(this, OpdetailActivity.class);
            intent.putExtra(INTENT_FRAG_CODE, code);
            intent.putExtra(INTENT_FRAG_TITLE, title);
            startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==Config.RESULT_TIMER){
            finish();
        }else{
            refreshTimer();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        handle.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        personel=null;
    }
}
