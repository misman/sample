package com.misman.staffoperations;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by misman on 24.04.2015.
 */
public class BootShutReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)){
            Intent intentStartApp = new Intent(context,LoginActivity.class);
            intentStartApp.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intentStartApp);
        }

    }
}
