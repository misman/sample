package com.misman.tool;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.misman.fragment.InOutEntry;
import com.misman.listeners.InOutListener;
import com.misman.model.Config;
import com.misman.model.InOut;
import com.misman.staffoperations.R;

import java.util.ArrayList;

/**
 * Created by misman on 30.03.2016.
 */
public class InOutListAdapter extends ArrayAdapter<InOut> {

    private ArrayList<InOut> inOuts;
    private InOutListener listener;
    private Activity act;
    private View.OnClickListener onClickBtnExcuse = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            InOut inOut = (InOut) v.getTag();
            listener.onItemClick(inOut);
        }
    };


    public InOutListAdapter(Activity context, InOutEntry fragment, ArrayList<InOut> objects) {
        super(context, R.layout.row_inout, objects);
        listener = fragment;
        act = context;
        inOuts = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        InOut inOut = getItem(position);
        if(convertView==null){
            LayoutInflater inflater = act.getLayoutInflater();
            view = inflater.inflate(R.layout.row_inout,null);
            holder = new ViewHolder();
            holder.txtDay = (TextView) view.findViewById(R.id.txt_day);
            holder.txtDate = (TextView) view.findViewById(R.id.txt_date);
            holder.txtIn = (TextView) view.findViewById(R.id.txt_in);
            holder.txtExcuseResult = (TextView) view.findViewById(R.id.txt_excuse_result);
            holder.txtOut = (TextView) view.findViewById(R.id.txt_out);
            holder.txtStat = (TextView) view.findViewById(R.id.txt_status);
            holder.txtWorkTime = (TextView) view.findViewById(R.id.txt_work_time);
            holder.btnExcuse = (Button) view.findViewById(R.id.btn_excuse);
            view.setTag(holder);
        }else{
            view = convertView;
        }
        holder = (ViewHolder) view.getTag();

        String[] datas = Config.sdf4.format(inOut.getDate()).split("-");
        holder.txtDay.setText(datas[1]);
        holder.txtDate.setText(datas[0]);
        holder.txtIn.setText(inOut.getInTime());
        holder.txtOut.setText(inOut.getOutTime());
        holder.txtWorkTime.setText(inOut.getWorkTime());
        holder.txtExcuseResult.setText(inOut.getExcuseResult());
        holder.txtStat.setText(inOut.getStatusName());
        holder.btnExcuse.setTag(inOut);
        holder.btnExcuse.setOnClickListener(onClickBtnExcuse);
        switch (inOut.getStatusCode()){
            case "0"://Devamsız
                holder.btnExcuse.setVisibility(View.VISIBLE);
                holder.txtStat.setTextColor(Color.parseColor("#FF4B3D"));
                break;
            case "1"://Uygun
                holder.btnExcuse.setVisibility(View.INVISIBLE);
                holder.txtStat.setTextColor(Color.parseColor("#92D787"));
                break;
            case "2"://Geç Giriş
                holder.btnExcuse.setVisibility(View.VISIBLE);
                holder.txtStat.setTextColor(Color.parseColor("#FFFA89"));
                break;
            case "3"://Erken Çıkış
                holder.btnExcuse.setVisibility(View.VISIBLE);
                holder.txtStat.setTextColor(Color.parseColor("#FFFA89"));
                break;
            case "4":
                holder.btnExcuse.setVisibility(View.VISIBLE);
                holder.txtStat.setTextColor(Color.parseColor("#FFD489"));
                holder.txtStat.setText("GG ve EÇ");//FFB289
                break;
        }
        if (inOut.getExcuse()!=null){
            holder.btnExcuse.setText(act.getString(R.string.title_excuse));
            holder.btnExcuse.setBackgroundResource(R.drawable.btn_excuse);
        }else {
            holder.btnExcuse.setText(act.getString(R.string.title_excuse_enter));
            holder.btnExcuse.setBackgroundResource(R.drawable.btn_excuse_enter);
        }
        if (inOut.getDayType().equals("0")) {
            holder.txtDay.setTextColor(Color.parseColor("#D46A6A"));
            holder.txtDate.setTextColor(Color.parseColor("#D46A6A"));
        }else {
            holder.txtDay.setTextColor(Color.parseColor("#FFFFFF"));
            holder.txtDate.setTextColor(Color.parseColor("#FFFFFF"));
        }
        return view;
    }

    @Override
    public InOut getItem(int position) {
        return inOuts.get(position);
    }

    static class ViewHolder {
        protected TextView txtDay;
        protected TextView txtDate;
        protected TextView txtIn;
        protected TextView txtStat;
        protected TextView txtExcuseResult;
        protected Button btnExcuse;
        protected TextView txtWorkTime;
        protected TextView txtOut;
    }
}
