package com.misman.tool;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by misman on 8.3.2016.
 */
public class Http {

    public enum RQ{GET,POST}

    private HttpURLConnection urlConnection;
    private String dataSend;

    public Http(String urlStr) {
        try {
            URL url = new URL(urlStr);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setRequestMethod(RQ.GET.name());
            urlConnection.setRequestProperty("charset", "utf-8");
            urlConnection.setReadTimeout(15 * 1000);
            urlConnection.setConnectTimeout(5 * 1000);
            urlConnection.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public HttpRes getContentAsStream(){
        HttpRes httpRes;
        try {
            int response = urlConnection.getResponseCode();
            httpRes = new HttpRes(response);
            httpRes.setContentStream(urlConnection.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return httpRes;
    }

    public void addRequestProp(String name,String value){
        urlConnection.setRequestProperty(name,value);
    }

    public Http(String urlStr, RQ rqType) {
        try {
            URL url = new URL(urlStr);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setRequestMethod(rqType.name());
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("charset", "utf-8");
            urlConnection.setReadTimeout(15 * 1000);
            urlConnection.setConnectTimeout(5*1000);
            if (rqType==RQ.POST) {
                urlConnection.setDoOutput(true);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public HttpRes startReq(){
        HttpRes httpRes=null;
        try {
            urlConnection.connect();
            if (dataSend!=null && !dataSend.equals("")) {
                DataOutputStream dos = new DataOutputStream(urlConnection.getOutputStream());
                dos.write(dataSend.getBytes("UTF8"));
                dos.close();
            }
            int response = urlConnection.getResponseCode();
            httpRes = new HttpRes(response);
            BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null)
            {
                sb.append(line);
            }
            httpRes.setData(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return httpRes;
    }

    public void setDataSend(String dataSend) {
        this.dataSend = dataSend;
    }

}
