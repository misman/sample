package com.misman.tool;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.misman.model.Config;
import com.misman.model.InOutDaily;
import com.misman.staffoperations.R;

import java.util.ArrayList;

/**
 * Created by misman on 30.03.2016.
 */
public class DailyInOutAdapter extends ArrayAdapter<InOutDaily> {

    private ArrayList<InOutDaily> inOuts;
    private Activity act;


    public DailyInOutAdapter(Activity context, ArrayList<InOutDaily> objects) {
        super(context, R.layout.row_inout_daily, objects);
        act = context;
        inOuts = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        InOutDaily inOut = getItem(position);
        if(convertView==null){
            LayoutInflater inflater = act.getLayoutInflater();
            view = inflater.inflate(R.layout.row_inout_daily,null);
            holder = new ViewHolder();
            holder.txtDay = (TextView) view.findViewById(R.id.txt_day);
            holder.txtMovement = (TextView) view.findViewById(R.id.txt_move);
            holder.txtHour = (TextView) view.findViewById(R.id.txt_hour);
            holder.txtPlace = (TextView) view.findViewById(R.id.txt_place);
            view.setTag(holder);
        }else{
            view = convertView;
        }
        holder = (ViewHolder) view.getTag();
        String[] datas = Config.sdf5.format(inOut.getTime()).split("-");
        holder.txtHour.setText(datas[1]);
        holder.txtDay.setText(datas[0]);
        if (inOut.getStatu().equals("1")) {
            holder.txtMovement.setTextColor(Color.parseColor("#84C68C"));
            holder.txtMovement.setText(act.getString(R.string.header_in));
        } else{
            holder.txtMovement.setTextColor(Color.parseColor("#D76D6D"));
            holder.txtMovement.setText(act.getString(R.string.header_out));
        }
        holder.txtPlace.setText(inOut.getPlace());
        return view;
    }

    @Override
    public InOutDaily getItem(int position) {
        return inOuts.get(position);
    }

    static class ViewHolder {
        protected TextView txtHour;
        protected TextView txtPlace;
        protected TextView txtMovement;
        protected TextView txtDay;
    }
}
