package com.misman.tool;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.misman.listeners.DayOffListener;
import com.misman.model.Config;
import com.misman.model.DayOff;
import com.misman.staffoperations.R;

import java.util.ArrayList;

/**
 * Created by misman on 11.3.2016.
 */
public class GeneralAdapter extends RecyclerView.Adapter<GeneralAdapter.Holder> {

    private DayOffListener listener;
    private ArrayList<DayOff> dayOffs;

    public GeneralAdapter(Fragment myCon, ArrayList<DayOff> objects) {
        dayOffs = objects;
        listener = (DayOffListener) myCon;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_dayoff, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        DayOff dayOff = dayOffs.get(position);
        String[] stDates = Config.sdf.format(dayOff.getBeginDate()).split("-");
        holder.txtStDay.setText(stDates[0]);
        holder.txtStMonth.setText(stDates[1]);
        holder.txtStYear.setText(stDates[2]);
        holder.txtEnd.setText(Config.sdf2.format(dayOff.getEndDate()));
        holder.txtCount.setText(String.valueOf(dayOff.getCount()));
        holder.txtTypeDesc.setText(dayOff.getTypeDesc());
        holder.txtConfirmDesc.setText(dayOff.getConfirmDesc());
        holder.txtConfRefDesc.setText(dayOff.getConfRefDesc());
        holder.txtRestmDesc.setText(dayOff.getDescription());

        holder.setBackground(R.drawable.used_list_item_background);
        holder.setOnClick(listener);
    }

    @Override
    public int getItemCount() {
        return dayOffs.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {

        private View thisView;
        protected TextView txtStDay;
        protected TextView txtStMonth;
        protected TextView txtStYear;
        protected TextView txtEnd;
        protected TextView txtCount;
        protected TextView txtTypeDesc;
        protected TextView txtConfirmDesc;
        protected TextView txtRestmDesc;
        protected TextView txtConfRefDesc;
        protected RelativeLayout relHolder;

        public Holder(View itemView) {
            super(itemView);
            thisView = itemView;
            txtStDay = (TextView) itemView.findViewById(R.id.txt_st_date_day);
            txtStMonth = (TextView) itemView.findViewById(R.id.txt_st_date_month);
            txtStYear = (TextView) itemView.findViewById(R.id.txt_st_date_year);
            txtEnd = (TextView) itemView.findViewById(R.id.txt_fn_date);
            txtCount = (TextView) itemView.findViewById(R.id.txt_count);
            txtTypeDesc = (TextView) itemView.findViewById(R.id.txt_type_desc);
            txtConfirmDesc = (TextView) itemView.findViewById(R.id.txt_confirm_desc);
            txtRestmDesc  = (TextView) itemView.findViewById(R.id.txt_rest_desc);
            txtConfRefDesc  = (TextView) itemView.findViewById(R.id.txt_conf_ref_desc);
            relHolder = (RelativeLayout) itemView.findViewById(R.id.rel_holder);
        }

        public void setBackground(int back){
            relHolder.setBackgroundResource(back);
        }

        public void setOnClick(final DayOffListener listener){
            thisView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(getAdapterPosition());
                }
            });
        }
    }


}
