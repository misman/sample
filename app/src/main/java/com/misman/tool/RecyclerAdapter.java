package com.misman.tool;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.misman.fragment.MenuFragment;
import com.misman.staffoperations.R;

/**
 * Created by misman on 9.2.2016.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.Holder> {

    public interface RecyclerClick {
        void onItemClick(int position);
    }
    private String[] menuItems;
    private RecyclerClick listener;

    public RecyclerAdapter(MenuFragment context, String[] items) {
        this.listener = context;
        this.menuItems = items;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menu_row, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        String data = menuItems[position];
        holder.txtTag.setText(data);
        holder.setOnClick(listener);
    }

    @Override
    public int getItemCount() {
        return menuItems.length;
    }


    public static class Holder extends RecyclerView.ViewHolder{

        private View thisView;
        protected TextView txtTag;

        public Holder(View itemView) {
            super(itemView);
            thisView = itemView;
            txtTag = (TextView) itemView.findViewById(R.id.txt_tag);
        }

        public void setOnClick(final RecyclerClick listener){
            thisView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(getAdapterPosition());
                }
            });
        }

    }
}
