package com.misman.tool;

import java.io.InputStream;

/**
 * Created by misman on 9.3.2016.
 */
public class HttpRes {

    private String data;
    private int response;
    private InputStream contentStream;

    public HttpRes(int response) {
        this.response = response;
    }

    public InputStream getContentStream() {
        return contentStream;
    }

    public void setContentStream(InputStream contentStream) {
        this.contentStream = contentStream;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getResponse() {
        return response;
    }

    public String getData() {
        return data;
    }
}
