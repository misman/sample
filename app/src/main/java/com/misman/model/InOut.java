package com.misman.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by misman on 30.03.2016.
 */
public class InOut {


    @SerializedName("PDKS_FARK")
    private String workTime;

    @SerializedName("PDKS_STATUS_NAME")
    private String statusName;

    @SerializedName("PDKS_STCODE")
    private String statusCode;

    @SerializedName("PDKS_XID")
    private String id;

    @SerializedName("PDKS_WORKDAY")
    private String dayType;

    @SerializedName("PDKS_M_STATU")
    private String excuseStatus;

    @SerializedName("PDKS_M_SONUC")
    private String excuseResult;

    @SerializedName("PDKS_P_DESC")
    private String excuse;

    @SerializedName("PDKS_DATE")
    private Date date;

    @SerializedName("PDKS_ILKGIRIS")
    private String inTime;

    @SerializedName("PDKS_SONCIKIS")
    private String outTime;

    public InOut(String id, String excuse) {
        this.excuse = excuse;
        this.id = id;
    }

    public String getWorkTime() {
        return workTime;
    }

    public String getStatusName() {
        return statusName;
    }

    public String getExcuse() {
        return excuse;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getId() {
        return id;
    }

    public String getDayType() {
        return dayType;
    }

    public String getExcuseStatus() {
        return excuseStatus;
    }

    public String getExcuseResult() {
        return excuseResult;
    }

    public Date getDate() {
        return date;
    }

    public String getOutTime() {
        return outTime;
    }

    public String getInTime() {
        return inTime;
    }
}
