package com.misman.model;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author misman
 */
public class Cryptor {
    
    private static final byte[] IV = "testtesttesttest".getBytes();
    private static final byte[] KEY = "thisisjustakeyyy".getBytes();
    
    public static String encrypt(String value) {
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(KEY, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(IV));

            byte[] encrypted = cipher.doFinal(value.getBytes("UTF-8"));
            String encStr = Base64.encodeToString(encrypted,Base64.NO_WRAP);
            return encStr;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    public static String decrypt(String encrypted) {
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(KEY, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(IV));
            byte[] original = cipher.doFinal(Base64.decode(encrypted,Base64.DEFAULT));
            String decrypted = new String(original);
            return decrypted;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }
    
}
