package com.misman.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by misman on 10.3.2016.
 */
public class Config {
    public static final String DEFAULT_BASE="http://misman.com.tr/rest/stuff/";
    public static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-yyyy-HH:mm",new Locale("tr"));
    public static final SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy HH:mm",new Locale("tr"));
    public static final SimpleDateFormat sdf3 = new SimpleDateFormat("ddMMyyyy",new Locale("tr"));
    public static final SimpleDateFormat sdf4 = new SimpleDateFormat("dd MMMM-EEEE",new Locale("tr"));
    public static final SimpleDateFormat sdf5 = new SimpleDateFormat("dd MMMM yyyy-HH:mm:ss",new Locale("tr"));
    public static final int RESULT_TIMER = 7;
    public static final int OPERATION_TIME = 90*1000;
    public static final String PREF_FILE_NAME = "conf";
    public static final String PREF_SERVICE_URL = "serv_url";

    public static class Personel{
        public String Pers_Code;
        public String Pers_Name;
        public String Dept_Name;
        public String Comp_Name;
        public String Dept_Code;
        public String Comp_Code;
    }

    public static class DayoffInfo{
        public String RestDay;
        public Date HireDate;
        public String ManagerName;
    }

}
