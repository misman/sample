package com.misman.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by misman on 4.04.2016.
 */
public class InOutDaily {

    @SerializedName("Daily_Place")
    private String place;

    @SerializedName("Daily_Statu")
    private String statu;

    @SerializedName("Daily_Time")
    private Date time;

    public Date getTime() {
        return time;
    }

    public String getPlace() {
        return place;
    }

    public String getStatu() {
        return statu;
    }
}
