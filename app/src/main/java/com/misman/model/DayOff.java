package com.misman.model;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.Date;

/**
 * Created by misman on 11.3.2016.
 */
public class DayOff implements Comparable<DayOff> {

    @SerializedName("Rest_Beg_Date")
    private Date beginDate;

    @SerializedName("Rest_End_Date")
    private Date endDate;

    @SerializedName("Rest_Day_Count")
    private int count;

    @SerializedName("Rest_Type_Desc")
    private String typeDesc;

    @SerializedName("Pers_Code")
    private String persCode;

    @SerializedName("Hrp20_Type")
    private String hrpType;

    @SerializedName("Rest_Conf_Desc")
    private String confirmDesc;

    @SerializedName("Rest_Code")
    private String restCode;

    @SerializedName("Left_Rest_Day")
    private int remainDay;

    @SerializedName("Rest_Desc")
    private String description;

    @SerializedName("ConfReF_Desc")
    private String confRefDesc;

    public DayOff(Date stDate, Date fnDate, String info) {
        beginDate = stDate;
        endDate = fnDate;
        description = info;
    }

    public boolean checkConflict(Date start, Date finish){
       if (confirmDesc.equals("Reddedildi")){
           return false;
        }
       try {
           Interval interval = new Interval(new DateTime(beginDate),new DateTime(endDate));
           DateTime startTime = new DateTime(start);
           DateTime finishTime = new DateTime(finish);
           return interval.contains(startTime) || interval.contains(finishTime);
       }catch (IllegalArgumentException e){
           return false;
       }

    }

    public void setConfRefDesc(String confRefDesc) {
        this.confRefDesc = confRefDesc;
    }

    public void setPersCode(String persCode) {
        this.persCode = persCode;
    }

    public void setHrpType(String hrpType) {
        this.hrpType = hrpType;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getTypeDesc() {
        return typeDesc;
    }

    public void setTypeDesc(String typeDesc) {
        this.typeDesc = typeDesc;
    }

    public String getConfirmDesc() {
        return confirmDesc;
    }

    public void setConfirmDesc(String confirmDesc) {
        this.confirmDesc = confirmDesc;
    }

    public String getRestCode() {
        return restCode;
    }

    public void setRestCode(String restCode) {
        this.restCode = restCode;
    }

    public int getRemainDay() {
        return remainDay;
    }

    public void setRemainDay(int remainDay) {
        this.remainDay = remainDay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getConfRefDesc() {
        return confRefDesc;
    }

    @Override
    public int compareTo(DayOff another) {
        return another.getBeginDate().compareTo(getBeginDate());
    }
}
