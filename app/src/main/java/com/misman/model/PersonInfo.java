package com.misman.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by misman on 23.3.2016.
 */
public class PersonInfo {

    @SerializedName("AD_SOYAD")
    public String name;

    @SerializedName("PERSONEL_TCNO")
    public String tcNo;

    @SerializedName("NUFUS_IL")
    public String idCity;

    @SerializedName("NUFUS_ILCE")
    public String idTown;

    @SerializedName("NUFUS_MAH")
    public String idDistrict;

    @SerializedName("NUFUS_KOY")
    public String idVillage;

    @SerializedName("NUFUS_CILT")
    public String idCilt;

    @SerializedName("NUFUS_SAYFA")
    public String idSayfa;

    @SerializedName("NUFUS_SIRA")
    public String idSira;

    @SerializedName("DOGUM_SEHIR")
    public String bornCity;

    @SerializedName("PERS_ANA_ADI")
    public String motherName;

    @SerializedName("PERS_BABA_ADI")
    public String fatherName;

    @SerializedName("KAN_GRUBU")
    public String bloodGroup;

    @SerializedName("CEP_TEL")
    public String mobPhone;

    @SerializedName("EV_TEL")
    public String homePhone;

    @SerializedName("EMAIL")
    public String email;

    @SerializedName("ULKE")
    public String country;

    @SerializedName("SEHIR")
    public String city;

    @SerializedName("ILCE")
    public String town;

    @SerializedName("POSTA_KODU")
    public String postCode;

    @SerializedName("ADRES")
    public String address;

    @SerializedName("SIRKET")
    public String company;

    @SerializedName("DEPARTMAN")
    public String department;

    @SerializedName("YONETICI")
    public String manager;

    @SerializedName("ISE_GIRIS_TARIHI")
    public Date dateEmployed;

    @SerializedName("MESLEK")
    public String profession;

    @SerializedName("YAKA_STATUSU")
    public String collarStatus;


}
